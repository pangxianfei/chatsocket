package chatsocket

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"gitee.com/pangxianfei/chatsocket/cache"
	"gitee.com/pangxianfei/chatsocket/chatservice"
	"gitee.com/pangxianfei/chatsocket/model"
	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

type UserMessage struct {
}

func (Um *UserMessage) GetMsgIdTime() (msgId string) {
	currentTime := time.Now().Nanosecond()
	msgId = fmt.Sprintf("%d", currentTime)
	return
}
func (Um *UserMessage) GetMsgTime() int64 {
	return time.Now().Unix()
}

// SendUserMessage 给用户发送消息
func (Um *UserMessage) SendUserMessage(msg *msgmodel.SendUserMsg, action string) (sendResults bool, err error) {
	sendResults = true
	servers, err := cache.GetServerAll()
	if err != nil {
		return
	}
	data := msgmodel.GetMsgData(action, msg.MsgId, msg.MsgType, msg.Message, msg.ToUid, msg.UserId, Um.GetMsgTime())
	for _, server := range servers {
		if chatservice.IsLocal(server) {
			SendMessages(msg.AppId, msg.ToUid, data)
		} else {
			RpcSendMessages(server, msg.AppId, msg.ToUid, data)
		}
	}
	return
}

// SendUserMessageLocal 给本机用户发送消息
func (Um *UserMessage) SendUserMessageLocal(appId int64, userId int64, data string) (sendResults bool, err error) {
	client := GetUserClient(appId, userId)
	if client == nil {
		err = errors.New("用户不在线")
		return
	}
	// 发送消息
	client.SendMsg([]byte(data))
	sendResults = true

	return
}

// SendUserMessageAll 给全体用户发消息
func (Um *UserMessage) SendUserMessageAll(msg *msgmodel.SendUserMsg, action string) (sendResults bool, err error) {
	if msg.GroupsId <= 0 {
		//1对1发送
		return Um.SendUserMessage(msg, action)
	}
	//群发送
	groupsUser := model.GetUsersAll(msg.GroupsId)
	for _, v := range groupsUser {
		msg.ToUid = int64(v.ID)
		_, _ = Um.SendUserMessage(msg, action)
	}
	return true, nil
}
