package cache

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitee.com/pangxianfei/frame/kernel/cache"
	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

const serversHashKey = "ws:hash:allservers" // 全部的服务器

func getServersHashKey() (key string) {
	return fmt.Sprintf("%s", serversHashKey)
}

// SetServerInfo 设置服务器信息
func SetServerInfo(server *msgmodel.Server) (err error) {
	var serversArr []*msgmodel.Server

	newData := GetServerCacheData()
	if newData != "" {
		err = json.Unmarshal([]byte(newData), &serversArr)
		return err
	}
	serversArr = append(serversArr, server)
	serverJson, err := json.Marshal(serversArr)
	if err != nil {
		return err
	}
	cache.SetNx(getServersHashKey(), string(serverJson))
	return
}

// DelServerInfo 下线服务器信息
func DelServerInfo(server *msgmodel.Server) (err error) {
	var serversArr []*msgmodel.Server

	newData := GetServerCacheData()
	if newData != "" {
		err = json.Unmarshal([]byte(newData), &serversArr)
		return err
	}

	for i, ser := range serversArr {
		if ser.Ip == server.Ip && ser.Port == server.Port {
			serversArr = append(serversArr[:i], serversArr[i+1:]...)
		}
	}
	serverJson, _ := json.Marshal(serversArr)
	cache.SetNx(getServersHashKey(), string(serverJson))
	return
}

func GetServerAll() (servers []*msgmodel.Server, err error) {

	newData := GetServerCacheData()
	if newData != "" {
		err = json.Unmarshal([]byte(newData), &servers)
	} else {
		serverMap := config.GetInterface("im.server")
		for _, v := range serverMap.(map[string]interface{}) {
			serversArr := strings.Split(v.(string), ":")
			if len(serversArr) == 2 {
				server2 := &msgmodel.Server{
					Ip:   serversArr[0],
					Port: serversArr[1],
				}
				servers = append(servers, server2)
			}
		}
		serverJson, _ := json.Marshal(servers)
		cache.SetNx(getServersHashKey(), string(serverJson))
	}
	return
}

func GetServerCacheData() (newData string) {
	newData = cache.GetString(getServersHashKey())
	if newData != "" {
		return ""
	}
	return newData
}
