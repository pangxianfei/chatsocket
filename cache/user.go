package cache

import (
	"encoding/json"
	"fmt"

	"gitee.com/pangxianfei/frame/kernel/cache"
	"github.com/pkg/errors"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

const userOnlinePrefix = "user:online:" // 用户在线状态

func getUserOnlineKey(userKey int64) string {
	return fmt.Sprintf("%s%d", userOnlinePrefix, userKey)
}
func GetUserOnlineInfo(userKey int64) (userOnline *msgmodel.UserOnline, err error) {
	key := getUserOnlineKey(userKey)
	newData := cache.GetString(key)
	userOnline = &msgmodel.UserOnline{}
	err = json.Unmarshal([]byte(newData), userOnline)
	if err != nil {
		return
	}
	return
}

// SetUserOnlineInfo 设置用户在线数据
func SetUserOnlineInfo(userKey int64, userOnline *msgmodel.UserOnline) (err error) {
	key := getUserOnlineKey(userKey)
	valueByte, err := json.Marshal(userOnline)
	cache.SetNx(key, string(valueByte))
	if cache.SetNx(key, string(valueByte)) {
		return nil
	}
	return errors.New("缓存失败")
}
