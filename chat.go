package chatsocket

import (
	"sync"

	"github.com/gorilla/websocket"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

const heartbeatExpirationTime = 6 * 60

var UserClients map[int64]*websocket.Conn

var Chat = new(UserMessage)

// ChatManager define a ws server manager
var ChatManager = ClientManager{
	Clients:     make(map[*Client]bool),
	UserClients: make(map[int64]*Client),
	SendUserMsg: make(chan *msgmodel.SendUserMsg),
	Register:    make(chan *Client),
	Login:       make(chan *UserLogin),
	Unregister:  make(chan *Client),
}

/************/

type DisposeFunc func(client *Client, seq string, message []byte) (code uint32, msg string, data interface{})

var handlerMap = make(map[string]DisposeFunc)
var handlerMapRWMutex sync.RWMutex
