package msgmodel

// Request 通用请求数据格式
type Request struct {
	Seq    string      `json:"seq"`            // 消息的唯一Id
	Action string      `json:"action"`         // 请求方法名
	Data   interface{} `json:"data,omitempty"` // 数据 json
}

// Login 接收用户登录请求数据
type Login struct {
	Token    string `json:"token,omitempty"` // token
	AppId    int64  `json:"appId,omitempty"`
	UserId   int64  `json:"userId,omitempty"`
	ToUid    int64  `json:"toUid,omitempty"`
	GroupsId int64  `json:"groupsId,omitempty"`
}

// HeartBeat 心跳请求数据
type HeartBeat struct {
	UserId string `json:"userId,omitempty"`
}

// SendUserMsg 发消息数据
type SendUserMsg struct {
	AppId    int64  `json:"appId,omitempty"`
	UserId   int64  `json:"userId,omitempty"`
	ToUid    int64  `json:"toUid,omitempty"`
	GroupsId int64  `json:"groupsId,omitempty"`
	MsgId    string `json:"msgId,omitempty"`
	MsgType  string `json:"msgType,omitempty"` //消息类型
	Message  string `json:"message,omitempty"`
}

// CheckLogin 验证登录用户
func (l *Login) CheckLogin() (ret bool) {
	ret = true
	if l.UserId <= 0 {
		ret = false
		return
	}
	return
}

// CheckToken 验证token
func (l *Login) CheckToken() (ret bool) {
	ret = true

	return
}
