package msgmodel

import "gitee.com/pangxianfei/chatsocket/library/response"

const (
	MessageTypeText  = "text"
	MessageTypeImage = "image"
	MessageTypeVideo = "video"
	MessageTypeVoice = "voice"

	MessageActionMsg       = "sendmsg"
	MessageActionEnter     = "enter"
	MessageActionExit      = "exit"
	MessageActionHeartbeat = "heartbeat"
)

//msgTimestamp  发送时间
//msgType  消息类型 text img
//content  内容
//from  发送者
//to  接受者
// 消息的定义

type Message struct {
	Action       string `json:"action"` //请求方法
	MsgId        string `json:"msgId"`
	MsgType      string `json:"msgType"`      // 消息类型 text/img/
	Content      string `json:"content"`      // 消息内容
	From         int64  `json:"from"`         // 发送者
	To           int64  `json:"to"`           // 接收者
	MsgTimestamp int64  `json:"msgTimestamp"` // 时间
}

func NewMessage(action, msgId string, msgType string, content string, to int64, from int64, msgTimestamp int64) (message *Message) {
	message = &Message{
		Action:       action,
		MsgId:        msgId,
		MsgType:      msgType,
		Content:      content,
		From:         from,
		To:           to,
		MsgTimestamp: msgTimestamp,
	}
	return
}

// GetMsgData 文本消息
func GetMsgData(action string, msgId string, msgType string, content string, to int64, from int64, msgTimestamp int64) string {
	data := NewMessage(action, msgId, msgType, content, to, from, msgTimestamp)
	res := NewResponse(response.OK, "success", data)
	return res.String()
}
