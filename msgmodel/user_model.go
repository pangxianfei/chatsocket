package msgmodel

import (
	"fmt"
	"time"

	"gitee.com/pangxianfei/frame/facades"
)

const (
	heartbeatTimeout = 3 * 60 // 用户心跳超时时间
)

// UserOnline 用户在线状态
type UserOnline struct {
	AccIp         string `json:"accIp"`         // acc Ip
	AccPort       string `json:"accPort"`       // acc 端口
	AppId         int64  `json:"appId"`         // appId
	UserId        int64  `json:"userId"`        // 用户Id
	ClientIp      string `json:"clientIp"`      // 客户端Ip
	ClientPort    string `json:"clientPort"`    // 客户端端口
	LoginTime     int64  `json:"loginTime"`     // 用户上次登录时间
	HeartbeatTime int64  `json:"heartbeatTime"` // 用户上次心跳时间
	LogOutTime    int64  `json:"logOutTime"`    // 用户退出登录的时间
	Qua           string `json:"qua"`           // qua
	DeviceInfo    string `json:"deviceInfo"`    // 设备信息
	IsLogoff      bool   `json:"isLogoff"`      // 是否下线
}

/**********************  数据处理  *********************************/

// UserLogin 用户登录
func UserLogin(accIp, accPort string, appId int64, userId int64, addr string, loginTime int64) (userOnline *UserOnline) {

	userOnline = &UserOnline{
		AccIp:         accIp,
		AccPort:       accPort,
		AppId:         appId,
		UserId:        userId,
		ClientIp:      addr,
		LoginTime:     loginTime,
		HeartbeatTime: loginTime,
		IsLogoff:      false,
	}

	return userOnline
}

// Heartbeat 用户心跳
func (u *UserOnline) Heartbeat(currentTime int64) {
	u.HeartbeatTime = currentTime
	u.IsLogoff = false
	return
}

// LogOut 用户退出登录
func (u *UserOnline) LogOut() {
	currentTime := time.Now().Unix()
	u.LogOutTime = currentTime
	u.IsLogoff = true
	return
}

/**********************  数据操作  *********************************/

// IsOnline 用户是否在线
func (u *UserOnline) IsOnline() bool {
	if u.IsLogoff {
		return false
	}
	currentTime := time.Now().Unix()
	if u.HeartbeatTime < (currentTime - heartbeatTimeout) {
		facades.Zap.Info(fmt.Sprintf("用户是否在线 心跳超时: %d - %d - %d", u.AppId, u.UserId, u.HeartbeatTime))
		return false
	}

	if u.IsLogoff {
		facades.Zap.Info(fmt.Sprintf("用户是否在线 用户已经下线: %d - %d", u.AppId, u.UserId))
		return false
	}
	return true
}

// UserIsLocal 用户是否在本台机器上
func (u *UserOnline) UserIsLocal(localIp, localPort string) bool {

	if u.AccIp == localIp && u.AccPort == localPort {
		return true
	}

	return false
}
