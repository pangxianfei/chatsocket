package chatsocket

import "time"

/**************************  manager info  ***************************************/
// 获取管理者信息

func GetManagerInfo(isDebug string) (managerInfo map[string]interface{}) {
	managerInfo = make(map[string]interface{})
	managerInfo["clientsLen"] = len(ChatManager.Clients)
	managerInfo["usersLen"] = len(ChatManager.UserClients)
	managerInfo["chanRegisterLen"] = len(ChatManager.Register)
	managerInfo["chanLoginLen"] = len(ChatManager.Login)
	managerInfo["chanUnregisterLen"] = len(ChatManager.Unregister)
	managerInfo["chanBroadcastLen"] = len(ChatManager.SendUserMsg)

	if isDebug == "true" {
		clients := make([]string, 0)
		for client := range ChatManager.Clients {
			clients = append(clients, client.Addr)
		}
		users := make([]int64, 0)
		for key := range ChatManager.UserClients {
			users = append(users, key)
		}
		managerInfo["clients"] = clients
		managerInfo["users"] = users
	}

	return
}

// GetUserClient 获取用户所在的连接
func GetUserClient(appId int64, userId int64) (client *Client) {
	client = ChatManager.GetUserClient(appId, userId)
	return
}

// ClearTimeoutConnections 定时清理超时连接
func ClearTimeoutConnections() {
	currentTime := time.Now().Unix()
	for client := range ChatManager.Clients {
		if client.IsHeartbeatTimeout(currentTime) {
			_ = client.Socket.Close()
		}
	}
}

// GetUserList 获取全部用户
func GetUserList() (userList []int64) {
	userList = make([]int64, 0)
	for _, v := range ChatManager.UserClients {
		userList = append(userList, v.UserId)
	}
	return
}

// AllSendMessages 全员广播
func AllSendMessages(appId int64, userId int64, data string) {
	ignore := ChatManager.GetUserClient(appId, userId)
	ChatManager.sendAll([]byte(data), ignore)
}

// SendMessages 发送消息
func SendMessages(appId int64, userId int64, data string) {
	client := ChatManager.GetUserClient(appId, userId)
	if client != nil {
		client.SendMsg([]byte(data))
	}
}
