package chatsocket

import (
	"sync"

	"gitee.com/pangxianfei/frame/facades"
	"github.com/gorilla/websocket"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

type Client struct {
	Addr          string          // 客户端地址
	Socket        *websocket.Conn // 用户连接
	Send          chan []byte     // 待发送的数据
	AppId         int64           // 登录的平台Id
	UserId        int64           // 用户Id
	ToUid         int64           // 接收用户Id
	GroupsId      int64           // 聊天组Id
	FirstTime     int64           // 首次连接事件
	HeartbeatTime int64           // 用户上次心跳时间
	LoginTime     int64           // 登录时间
	ClientsLock   sync.RWMutex    // 读写锁
}

// AddClients 添加客户端
func (c *Client) AddClients(userId int64, conn *websocket.Conn) {
	c.ClientsLock.Lock()
	defer c.ClientsLock.Unlock()
	UserClients[userId] = conn
}

func (c *Client) Read() {
	defer func() {
		if r := recover(); r != nil {
			facades.Zap.Error(r.(string))
		}
	}()
	defer func() {
		ChatManager.Unregister <- c
		_ = c.Socket.Close()
	}()

	for {
		c.Socket.PongHandler()
		_, message, err := c.Socket.ReadMessage()
		if err != nil {
			ChatManager.Unregister <- c
			_ = c.Socket.Close()
			break
		}
		//处理客户端信息
		Handle(c, message)
	}
}

func (c *Client) Write() {
	defer func() {
		_ = c.Socket.Close()
	}()

	for {
		select {
		case message, ok := <-c.Send:
			if !ok {
				_ = c.Socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			//发送到到客户端的信息
			_ = c.Socket.WriteMessage(websocket.TextMessage, message)
		}
	}
}

func (c *Client) SendMsg(msg []byte) {
	if c == nil {
		return
	}
	defer func() {
		if r := recover(); r != nil {
			facades.Zap.Warn("SendMsg stop:" + r.(string))
		}
	}()
	c.Send <- msg
}

func (c *Client) Login(request *msgmodel.Login, loginTime int64) {
	c.AppId = request.AppId
	c.UserId = request.UserId
	c.ToUid = request.ToUid
	c.GroupsId = request.GroupsId
	c.LoginTime = loginTime
	// 登录成功=心跳一次
	c.Heartbeat(loginTime)
}

func (c *Client) Heartbeat(currentTime int64) {
	c.HeartbeatTime = currentTime
	return
}

func (c *Client) IsHeartbeatTimeout(currentTime int64) bool {
	if c.HeartbeatTime+heartbeatExpirationTime <= currentTime {
		return true
	}
	return false
}

// IsLogin 是否登录了
func (c *Client) IsLogin() bool {
	return c.UserId > 0
}

// GetKey 读取客户端数据
func (c *Client) GetKey() (key int64) {
	return GetUserKey(c.AppId, c.UserId)
}
