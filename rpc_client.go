package chatsocket

import (
	"context"
	"log"

	"gitee.com/pangxianfei/frame/library/config"
	"github.com/smallnest/rpcx/client"
	"github.com/smallnest/rpcx/protocol"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

type Args struct {
	AppId  int64  `json:"appId,omitempty"`
	UserId int64  `json:"userId,omitempty"`
	Data   string `json:"data,omitempty"`
}
type Reply struct{}

func RpcSendMessages(server *msgmodel.Server, appId int64, userId int64, data string) {
	rpcConnect := RpcConnect("RpcServer", server.Ip)
	defer func(rpcConnect client.XClient) {
		_ = rpcConnect.Close()
	}(rpcConnect)
	args := Args{
		AppId:  appId,
		UserId: userId,
		Data:   data,
	}
	reply := &Reply{}
	err := rpcConnect.Call(context.Background(), "SendMessages", args, reply)
	if err != nil {
		log.Fatalf("failed to call: %v", err)
	}
}
func RpcConnect(servicePath, ip string) client.XClient {
	d, _ := client.NewPeer2PeerDiscovery("tcp@"+ip+":"+config.GetString("im.rpc_port"), "")
	opt := client.DefaultOption
	opt.SerializeType = protocol.JSON
	return client.NewXClient(servicePath, client.Failtry, client.RandomSelect, d, opt)
}
