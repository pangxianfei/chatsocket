package chatservice

import (
	"gitee.com/pangxianfei/frame/library/config"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

func GetAppIds() []int64 {
	return []int64{1, 2}
}

func GetServer() (server *msgmodel.Server) {
	server = msgmodel.NewServer(config.GetString("im.ip"), config.GetString("im.port"))
	return
}

func IsLocal(server *msgmodel.Server) (isLocal bool) {
	if server.Ip == config.GetString("im.ip") && server.Port == config.GetString("im.port") {
		isLocal = true
	}
	return
}

func InAppIds(appId int64) bool {
	for _, value := range GetAppIds() {
		if value == appId {
			return true
		}
	}
	return false
}
