package contracts

import (
	"context"

	"gitee.com/pangxianfei/chatsocket"
)

type rpcServerBase interface {
	SendMessages(ctx context.Context, args chatsocket.Args, reply *chatsocket.Reply) error
}
