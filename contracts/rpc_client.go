package contracts

import (
	"github.com/gorilla/websocket"

	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

type rpcClientBase interface {
	AddClients(userId int64, conn *websocket.Conn)
	Read()
	Write()
	SendMsg(msg []byte)
	Login(request *msgmodel.Login, loginTime int64)
	Heartbeat(currentTime int64)
	IsHeartbeatTimeout(currentTime int64) bool
	IsLogin() bool
	GetKey() (key int64)
}
