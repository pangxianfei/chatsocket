package contracts

import (
	"gitee.com/pangxianfei/chatsocket"
	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

type ManagerClientBase interface {
	AddClients(client *chatsocket.Client)
	DelClients(client *chatsocket.Client)
	GetUserClient(appId int64, userId int64) (client *chatsocket.Client)
	AddUsers(key int64, client *chatsocket.Client)
	DelUsers(key int64)
	sendAll(message []byte, ignore *chatsocket.Client)
	EventSendUserMsg(message *msgmodel.SendUserMsg)
	EventRegister(client *chatsocket.Client)
	EventLogin(login **chatsocket.UserLogin)
	EventUnregister(client **chatsocket.Client)
}
