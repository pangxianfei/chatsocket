package run

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"

	"gitee.com/pangxianfei/frame/request"

	"gitee.com/pangxianfei/chatsocket"
)

func CreateWsServer(ctx request.Context) {
	conn, err := (&websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).Upgrade(ctx.Writer(), ctx.Request(), nil)
	if err != nil {
		http.NotFound(ctx.Writer(), ctx.Request())
		return
	}
	conn.CloseHandler()
	currentTime := time.Now().Unix()
	//可以添加用户信息验证
	client := &chatsocket.Client{
		Addr:          conn.RemoteAddr().String(),
		Socket:        conn,
		Send:          make(chan []byte, 100),
		FirstTime:     currentTime,
		HeartbeatTime: currentTime,
	}
	go client.Read()
	go client.Write()
	chatsocket.ChatManager.Register <- client
	return
}
