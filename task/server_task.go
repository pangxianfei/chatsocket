package task

import (
	"time"

	"gitee.com/pangxianfei/frame/facades"

	"gitee.com/pangxianfei/chatsocket/cache"
	"gitee.com/pangxianfei/chatsocket/chatservice"
)

func ServerInit() {
	Timer(2*time.Second, 60*time.Second, server, "", serverDefer, "")
}

// 服务注册
func server(param interface{}) (result bool) {
	result = true
	defer func() {
		if r := recover(); r != nil {
			facades.Zap.Info("服务注册 stop" + r.(string))
		}
	}()
	server := chatservice.GetServer()
	_ = cache.SetServerInfo(server)
	return
}

// 服务下线
func serverDefer(param interface{}) (result bool) {
	defer func() {
		if r := recover(); r != nil {
			facades.Zap.Info("服务下线 stop" + r.(string))
		}
	}()
	server := chatservice.GetServer()
	_ = cache.DelServerInfo(server)

	return
}
