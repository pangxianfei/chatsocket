package task

import (
	"runtime/debug"
	"time"

	"gitee.com/pangxianfei/frame/facades"

	"gitee.com/pangxianfei/chatsocket"
)

func Init() {
	Timer(3*time.Second, 30*time.Second, cleanConnection, "", nil, nil)

}

// 清理超时连接
func cleanConnection(param interface{}) (result bool) {
	result = true
	defer func() {
		if r := recover(); r != nil {
			facades.Zap.Info("ClearTimeoutConnections stop" + string(debug.Stack()))
		}
	}()
	chatsocket.ClearTimeoutConnections()
	return
}
