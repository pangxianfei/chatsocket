package chatsocket

// UserLogin 用户登录
type UserLogin struct {
	AppId  int64
	UserId int64
	Client *Client
}

// GetKey 读取客户端数据
func (l *UserLogin) GetKey() (key int64) {
	return GetUserKey(l.AppId, l.UserId)
}

// GetUserKey 获取用户key
func GetUserKey(appId int64, userId int64) (key int64) {
	return userId
}
