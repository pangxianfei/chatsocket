package chatsocket

import (
	"fmt"
	"sync"

	"gitee.com/pangxianfei/frame/facades"

	"gitee.com/pangxianfei/chatsocket/cache"
	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

// ClientManager is a websocket manager
type ClientManager struct {
	Clients         map[*Client]bool // 全部的连接
	ClientsLock     sync.RWMutex     // 读写锁
	UserClients     map[int64]*Client
	UserClientsLock sync.RWMutex               // 读写锁
	SendUserMsg     chan *msgmodel.SendUserMsg //广播消息
	Register        chan *Client               //连接
	Login           chan *UserLogin            // 用户登录处理
	Unregister      chan *Client               //退出
}

// AddClients 添加客户端
func (manager *ClientManager) AddClients(client *Client) {
	manager.ClientsLock.Lock()
	defer manager.ClientsLock.Unlock()
	manager.Clients[client] = true
}

// DelClients 删除客户端
func (manager *ClientManager) DelClients(client *Client) {
	manager.ClientsLock.Lock()
	defer manager.ClientsLock.Unlock()
	delete(manager.Clients, client)
}

// GetUserClient 获取用户的连接
func (manager *ClientManager) GetUserClient(appId int64, userId int64) (client *Client) {
	manager.UserClientsLock.RLock()
	defer manager.UserClientsLock.RUnlock()
	userKey := GetUserKey(appId, userId)
	if value, ok := manager.UserClients[userKey]; ok {
		client = value
	}
	return
}

// AddUsers 添加用户
func (manager *ClientManager) AddUsers(key int64, client *Client) {
	manager.UserClientsLock.Lock()
	defer manager.UserClientsLock.Unlock()
	manager.UserClients[key] = client
}

// DelUsers 删除用户
func (manager *ClientManager) DelUsers(key int64) {
	manager.UserClientsLock.Lock()
	defer manager.UserClientsLock.Unlock()

	delete(manager.UserClients, key)
}

// 向全部成员(除了自己)发送数据
func (manager *ClientManager) sendAll(message []byte, ignore *Client) {
	for conn := range manager.Clients {
		if conn != ignore {
			conn.SendMsg(message)
		}
	}
}

// EventSendUserMsg 发送用户信息
func (manager *ClientManager) EventSendUserMsg(message *msgmodel.SendUserMsg) {
	manager.ClientsLock.RLock()
	defer manager.ClientsLock.RUnlock()
	if message.ToUid <= 0 {
		facades.Zap.Error(fmt.Sprintf("接收方不存在: %d - %d", message.AppId, message.UserId))
		return
	}
	_, _ = Chat.SendUserMessageAll(message, msgmodel.MessageActionMsg)
}

// EventRegister 用户建立连接事件
func (manager *ClientManager) EventRegister(client *Client) {
	manager.AddClients(client)
	data := msgmodel.GetMsgData(msgmodel.MessageActionEnter, Chat.GetMsgIdTime(), msgmodel.MessageTypeText, "连接成功", client.ToUid, client.UserId, Chat.GetMsgTime())
	client.Send <- []byte(data)
}

// EventLogin 用户登录
func (manager *ClientManager) EventLogin(login *UserLogin) {
	manager.ClientsLock.RLock()
	defer manager.ClientsLock.RUnlock()
	// 连接存在，在添加
	if _, ok := manager.Clients[login.Client]; ok {
		userKey := login.GetKey()
		manager.AddUsers(userKey, login.Client)
	}
}

// EventUnregister 用户断开连接
func (manager *ClientManager) EventUnregister(client *Client) {
	manager.DelClients(client)
	// 删除用户连接
	userKey := GetUserKey(client.AppId, client.UserId)
	manager.DelUsers(userKey)
	// 清除redis登录数据
	userOnline, err := cache.GetUserOnlineInfo(client.GetKey())
	if err == nil {
		userOnline.LogOut()
		_ = cache.SetUserOnlineInfo(client.GetKey(), userOnline)
	}
	if client.UserId > 0 {
		var message *msgmodel.SendUserMsg
		message = new(msgmodel.SendUserMsg)
		message.AppId = client.AppId
		message.UserId = client.UserId
		message.GroupsId = client.GroupsId
		message.MsgId = Chat.GetMsgIdTime()
		message.Message = "用户已经离开"
		message.MsgType = msgmodel.MessageTypeText
		_, _ = Chat.SendUserMessageAll(message, msgmodel.MessageActionExit)
	}
}
