package chatsocket

import (
	"context"
)

type RpcServer struct{}

func (R *RpcServer) SendMessages(ctx context.Context, args Args, reply *Reply) error {
	SendMessages(args.AppId, args.UserId, args.Data)
	return nil
}
