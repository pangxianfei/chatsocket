package chatsocket

// Start 管道处理程序
func (manager *ClientManager) Start() {
	for {
		select {
		case conn := <-manager.Register:
			// 建立连接事件
			manager.EventRegister(conn)

		case login := <-manager.Login:
			// 用户登录
			manager.EventLogin(login)

		case conn := <-manager.Unregister:
			// 断开连接事件
			manager.EventUnregister(conn)

		case sendUserMsg := <-manager.SendUserMsg:
			//聊天室广播
			manager.EventSendUserMsg(sendUserMsg)
		}
	}
}
