package chatsocket

import (
	"encoding/json"
	"fmt"

	"gitee.com/pangxianfei/chatsocket/library/response"
	"gitee.com/pangxianfei/chatsocket/msgmodel"
)

// Register 注册
func Register(key string, value DisposeFunc) {
	handlerMapRWMutex.Lock()
	defer handlerMapRWMutex.Unlock()
	handlerMap[key] = value
	return
}

func gethandlerMap(key string) (value DisposeFunc, ok bool) {
	handlerMapRWMutex.RLock()
	defer handlerMapRWMutex.RUnlock()
	value, ok = handlerMap[key]
	return
}

// Handle 处理数据
func Handle(client *Client, message []byte) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("处理数据 stop", r)
		}
	}()
	request := &msgmodel.Request{}

	err := json.Unmarshal(message, request)
	if err != nil {
		client.SendMsg([]byte("数据不合法"))
		return
	}

	requestData, err := json.Marshal(request.Data)
	if err != nil {
		client.SendMsg([]byte("处理数据失败"))
		return
	}
	seq := request.Seq
	action := request.Action
	var code uint32
	var msg string
	var data interface{}
	// 获取所有处理方式
	if value, ok := gethandlerMap(action); ok {
		code, msg, data = value(client, seq, requestData)
	} else {
		code = response.RoutingNotExist
		fmt.Println("处理数据 路由不存在", client.Addr, "action", action)
	}
	msg = response.GetErrorMessage(code, msg)
	responseHead := msgmodel.NewResponseHead(seq, action, code, msg, data)
	headByte, err := json.Marshal(responseHead)
	if err != nil {
		return
	}
	client.SendMsg(headByte)
	return
}
