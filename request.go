package chatsocket

type HeaderToken struct {
	Authorization string `header:"Authorization" binding:"required,min=20"`
	UserId        int64  `header:"UserId" binding:"required"`
}
